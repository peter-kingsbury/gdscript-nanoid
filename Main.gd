extends Node


func _ready() -> void:
	var timer = Timer.new()
	timer.connect("timeout", self, "_on_Timer_timeout")
	add_child(timer)
	timer.start()


func _on_Timer_timeout() -> void:
	print(Nanoid.nanoid())

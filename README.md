# gdscript-nanoid

A tiny, secure, URL-friendly, unique string ID generator for Godot Game Engine, written in GDScript.

For a more complete breakdown of all that is Nanoid, please see the original [project](https://github.com/ai/nanoid).

# Usage

## Installation

* Clone this repository.
* Copy the file `nanoid.gd` to your own Godot project.
* Under Project | Project Settings | AutoLoad:
	* Choose the Path containing `nanoid.gd`.
	* Click Add.

## Basic Usage

Now, when you want to generate a Nanoid from anywhere in your application, simply use:
```gdscript
print(Nanoid.nanoid())

# Mvm_aGUfvr
```

## Custom Length

Before generating any Nanoids, call the initializer function to set the ID length you'd like to use.

Providing `null` will fallback to the default length (`10`).

```gdscript
var my_length = 16
Nanoid.initialize(my_length)
print(Nanoid.nanoid())

# qE8qLC8lFJkErq_h
```

## Custom Alphabet

Before generating any Nanoids, call the initializer function with a length and a custom alphabet that you'd like to use.


Providing `null` will fallback to the default length (`10`) and default alphabet (`abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_`).

```gdscript
var my_length = 8
var my_alphabet = "abcdef0123456789"
Nanoid.initialize(my_length, my_alphabet)
print(Nanoid.nanoid())

# 3cf2f7be
```

## Custom Random Seed

Before generating any Nanoids, call the initializer function with a length, custom alphabet, and seed value that you'd like to use.

Additionally, if you'd like to restore the state of a previously-used seed, provide the seed-state value as a fourth parameter.

```gdscript
var my_length = 3
var my_alphabet = "0123456789"
var my_seed = "same every time"
Nanoid.initialize(my_length, my_alphabet, my_seed)

print(Nanoid.nanoid())
print(Nanoid.nanoid())
print(Nanoid.nanoid())

# 073
# 388
# 707
```

By calling `Nanoid.get_seed_state()` you can save and restore the state and last known seed value program executions, allowing you to reproduce any sequence of randomly-generated numbers, using your own seed for the genesis of the sequence.
